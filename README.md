# Jueves

* [!] AUTOR: Manza
* [!] Version: 1.2.0
* [!] Last Release: 15 August of 2018
* [!] Nª Tools: 4
* [!] Tested in Python 3.6 & Python 3.7

It is a tool destined for the MITM with a set of tools, for now lacks of enough tools, but if they will implement little by little. For now it has [ARP poison, sniff DNS, Route] The program is functional in both Windows and Linux, although it was destined to Linux.

Currently the code works on both Windows and Linux including Termux (Android), is still working for more information about the functions of the project look at the Wiki

**Install all the requeriments. [pip install -r requeriments.txt]**

_See the jueves wiki for more information_

-Manza
